# requires newer autoconf version than on Ubuntu18 apt
sudo chmod -R 777 /opt /var/tmp && cd /opt
cd /opt
wget http://ftp.gnu.org/gnu/autoconf/autoconf-2.71.tar.gz 
tar xf autoconf*
cd autoconf-2.71
sh configure --prefix /usr/local
sudo make install

cd /opt && git clone https://github.com/esnet/iperf && cd iperf
sudo bash bootstrap.sh && sudo bash configure && sudo make && sudo make install
sudo /opt/iperf/src/iperf3 --bind-dev oaitun_ue1 -c 192.168.70.135

sudo add-apt-repository ppa:deadsnakes/ppa
sudo apt-get update
sudo apt-get install -y python3.9   # must use python3.9 to call
echo done
